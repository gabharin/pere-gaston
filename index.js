const { token } = require("./config.json");
const { Client, GatewayIntentBits, Events, Partials } = require("discord.js");

const client = new Client({
  partials: [Partials.Channel, Partials.Message],
  intents: [
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.DirectMessageTyping,
  ],
});

const CHANNEL_LIST = ["1223292752079622245", "1223672620340482098"];

client.on(Events.MessageCreate, async (message) => {
  let attachments = [];
  if (message.attachments) {
    message.attachments.map((a) => {
      attachments.push(a.attachment);
    });
  }
  CHANNEL_LIST.map(async (c) => {
    const channel = await client.channels.fetch(c);
    if (message.content) {
      channel.send({ content: message.content, files: attachments });
    }
    if (!message.content && attachments) {
      channel.send({ files: attachments });
    }
  });
});

client.login(token);
